#ifndef AREA_H
#define AREA_H

#define pi 3.1415

float areatriangulo (float base, float altura);
float arearetangulo (float base, float altura);
float areaquadrado (float lado);
float areacirculo (float raio);
float areapiramide (float areabase, float arealateral);
float areacubo (float aresta);
float areaparalelepipedo (float aresta1, float aresta2, float aresta3);
float areaesfera (float raio);

#endif
