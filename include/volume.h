#ifndef VOLUME_H
#define VOLUME_H

float volumepiramide(float areabase, float altura);

float volumecubo (float aresta);

float volumeparalelepipedo (float aresta1, float aresta2, float aresta3);

float volumeesfera (float raio);

#endif