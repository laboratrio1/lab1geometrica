#ifndef PERIMETRO_H
#define PERIMETRO_H

#define pi 3.1415

float perimetrotriangulo (float lado);

float perimetroretangulo (float base, float altura);

float perimetroquadrado (float lado);

float perimetrocirculo (float raio);

#endif
