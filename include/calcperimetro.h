#ifndef CALCPERIMETRO_H
#define CALCPERIMETRO_H

#include "perimetro.h"

float calcperimetrotriangulo();
float calcperimetroretangulo();
float calcperimetroquadrado();
float calcperimetrocirculo();

#endif