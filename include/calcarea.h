#ifndef CALCAREA_H
#define CALCAREA_H
#include "area.h"

float calcareatriangulo();
float calcarearetangulo();
float calcareaquadrado();
float calcareacirculo();
float calcareapiramide();
float calcareacubo();
float calcareaparalelepipedo();
float calcareaesfera();
#endif