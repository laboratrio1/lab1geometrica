#ifndef CALCVOLUME_H
#define CALCVOLUME_H
#include "volume.h"

float calcvolumepiramide();
float calcvolumecubo();
float calcvolumeparalelepipedo();
float calcvolumeesfera();

#endif