#include <iostream>
#include "perimetro.h"

float perimetrotriangulo (float lado){
	float resposta;
	resposta = 3 * lado;
	return resposta;
}

float perimetroretangulo (float base, float altura){
	float resposta;
	resposta = 2 * (base + altura);
	return resposta;
}

float perimetroquadrado (float lado){
	float resposta;
	resposta = 4 * lado;
	return resposta;
}

float perimetrocirculo (float raio){
	float resposta;
	resposta = 2 * pi * raio;
	return resposta;
}
