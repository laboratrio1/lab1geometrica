#include <iostream>
#include "calcarea.h"
#include "area.h"

using std::cin;
using std::cout;
using std::endl;

float calcareatriangulo(){
	float base, altura;
	cout << "Para calcular a area do triangulo equilatero digite a medida do lado: " << endl;
	cin >> base;
	cout << "Para calcular a area do triangulo equilatero, digite a altura dele: " << endl;
	cin >> altura;
	return areatriangulo(base, altura);
}

float calcarearetangulo(){
	float base, altura;
	cout << "Para calcular a area do retangulo, digite a medida da base: " << endl;
	cin >> base;
	cout << "Para calcular a area do retangulo, digite a medida da altura: " << endl;
	cin >> altura;
	return arearetangulo(base, altura);
}

float calcareaquadrado(){
	float lado;
	cout << "Para calcular a area do quadrado, digite a medida do lado: " << endl;
	cin >> lado;
	return areaquadrado(lado);
}

float calcareacirculo(){
	float raio;
	cout << "Para calcular a area do circulo, digite a medida do raio: " << endl;
	cin >> raio;
	return areacirculo(raio);
}

float calcareapiramide(){
	float areabase, arealateral;
	cout << "Para calcular a area da piramide, digite a área de sua base quadrangular: " << endl;
	cin >> areabase;
	cout << "Para calcular a area da piramide, digite sua área lateral: " << endl;
	cin >> arealateral;
	return areapiramide(areabase, arealateral);
}

float calcareacubo(){
	float aresta;
	cout << "Para calcular a area do cubo, digite o valor da aresta: " << endl;
	cin >> aresta;
	return areacubo(aresta);
}

float calcareaparalelepipedo(){
	float aresta1, aresta2, aresta3;
	cout << "Para calcular a area do paralelepípedo, digite a medida da 1ª aresta: " << endl;
	cin >> aresta1;
	cout << "Para calcular a area do paralelepípedo, digite a medida da 2ª aresta: " << endl;
	cin >> aresta2;
	cout << "Para calcular a area do paralelepípedo, digite a medida da 3ª aresta: " << endl;
	cin >> aresta3;
	return areaparalelepipedo(aresta1, aresta2, aresta3);
}
float calcareaesfera(){
	float raio;
	cout << "Para calcular a area da esfera, digite a medida do raio: " << endl;
	cin >> raio;
	return areaesfera(raio);
}


