/**
 * @file main.cpp
 * @brief Programa que calcula área, perimetro ou volume de figuras geométricas
 *@author Marcus Maia e Maíla Alves
 *@since 09/03/2017
 *@date 12/03/2017
 */
#include <iostream>
#include <cstdlib>
#include "area.h"
#include "perimetro.h"
#include "calcarea.h"
#include "calcperimetro.h"
#include "calcvolume.h"
#include "volume.h"
/**
 * @brief    Funcao principal (main)
 * @return 0
 */

using std::cin;
using std::cout;
using std::endl;
/** @brief definicao das variaveis que vao ser utilizadas */
int main(){

    int opcao;
    float resultarea, resultperimetro, resultvolume;

	//Menu Inicial
	cout << "Calculadora de Geometria Plan e Espacial" << endl;
	cout << "(1) Triangulo equilatero" << endl;
	cout << "(2) Retangulo" << endl;
	cout << "(3) Quadrado" << endl;
	cout << "(4) Circulo" << endl;
	cout << "(5) Piramide com base quadrangular" << endl;
	cout << "(6) Cubo" << endl;
	cout << "(7) Paralelepipedo" << endl;
	cout << "(8) Esfera" << endl;
	cout << "(0) Sair" << endl << endl;
	cout << "Digite a sua opção: "<< endl;
	
	cin >> opcao;
    /** @dbrief estrutura condicional, se a opção digitada é inválida, executa-se um laco condicional ate que o usuario digite uma opcao valida. */

	if (isdigit(opcao) == true || opcao > 8 || 0 > opcao){
		/** @brief laco condicional usado ate que o usuario digite uma opcao valida */
        while (isdigit(opcao) == true || opcao > 8 || 0 > opcao){
			cout << "Opção inválida! Por favor, digite novamente: " << endl;
			cin >> opcao;
		}
	}
    /** @brief estruturas condicionais utilizadas para opções válidas
     * @brief a primeira, caso opcao seja igua a zero, para o programa
     */
    //Opção escolhida
    if (opcao == 0){
    	cout << "Seção encerrada!" << endl;
        exit(0);
    }
    /** @brief caso a opcao seja 1, calcula area e perimetro do triangulo equilatero*/
    if (opcao == 1){
		resultarea = calcareatriangulo();
    	resultperimetro = calcperimetrotriangulo();
    	cout << "A área do triangulo é: " << resultarea << endl << endl;
    	cout << "O perímetro do triangulo é: " << resultperimetro << endl << endl;
    }
     /** @brief caso a opcao seja 2, calcula area e perimetro do retangulo*/
   	if (opcao == 2){
       

		resultarea = calcarearetangulo();
    	resultperimetro = calcperimetroretangulo();
    	cout << "A área do retangulo é: " << resultarea << endl << endl;
    	cout << "O perímetro do retangulo é: " << resultperimetro << endl << endl;
    }
    /** @brief caso a opcao seja 3, calcula area e perimetro do quadrado*/
    if (opcao == 3){
		resultarea = calcareaquadrado();
    	resultperimetro = calcperimetroquadrado();
    	cout << "A área do quadrado é: " << resultarea << endl << endl;
    	cout << "O perímetro do quadrado é: " << resultperimetro << endl << endl;
    }
     /** @brief caso a opcao seja 4, calcula area e perimetro do circulo*/
    if (opcao == 4){
		resultarea = calcareacirculo();
    	resultperimetro = calcperimetrocirculo();
    	cout << "A área do círculo é: " << resultarea << endl << endl;
    	cout << "O perímetro do círculo é: " << resultperimetro << endl << endl;
    }
     /** @brief caso a opcao seja 5, calcula area e volume da piramide*/
    if (opcao == 5){
        resultarea = calcareapiramide();
        resultvolume = calcvolumepiramide();
        cout << "A área da pirâmide é: " << resultarea << endl << endl;
        cout << "O volume da pirâmide é: " << resultvolume << endl << endl;
    }
     /** @brief caso a opcao seja 6, calcula area e volume do cubo*/
    if (opcao == 6){
        resultarea = calcareacubo();
        resultvolume = calcvolumecubo();
        cout << "A área do cubo é: " << resultarea << endl << endl;
        cout << "O volume do cubo é: " << resultvolume << endl << endl;
    }
     /** @brief caso a opcao seja 7, calcula area e volume do paralelepipedo*/
    if (opcao == 7){
        resultarea = calcareaparalelepipedo();
        resultvolume = calcvolumeparalelepipedo();
        cout << "A área do paralelepípedo é: " << resultarea << endl << endl;
        cout << "O volume do paralelepípedo é: " << resultvolume << endl << endl;
    }
    /** @brief caso a opcao seja 8, calcula area e volume da esfera*/
    if (opcao == 8){
        resultarea = calcareaesfera();
        cout << "A área da esfera é: " << resultarea << endl << endl;
        resultvolume = calcvolumeesfera();
        cout << "o volume da esfera é: " << resultvolume << endl << endl;

    }

return 0;
}
