#include <iostream>
#include <cstdlib>
#include <cmath>
#include "area.h"

float areatriangulo (float base, float altura){
	float resultado;
	resultado = (base * altura) / 2;
	return resultado;
}

float arearetangulo (float base, float altura){
	float resultado;
	resultado = (base * altura);
	return resultado;
}

float areaquadrado (float lado){
	float resultado;
	resultado = pow (2, lado);
	return resultado;
}

float areacirculo (float raio){
	float resultado;
	resultado = pi * (pow (2, raio));
	return resultado;
}

float areapiramide (float areabase, float arealateral){
	float resultado;
	resultado = areabase + arealateral;
	return resultado;
}

float areacubo (float aresta){
	float resultado;
	resultado = 6 * (pow(2, aresta));
	return resultado;
}

float areaparalelepipedo(float aresta1,float aresta2,float aresta3){
	float resultado;
	resultado = (2 * aresta1 * aresta2) + (2 * aresta1 * aresta3) + (2 * aresta2 * aresta3);
	return resultado;

}
float areaesfera (float raio){
	float resultado;
	resultado = 4 * (pi * pow(2,raio));
	return resultado;
}
