#include <iostream>
#include "calcperimetro.h"
#include "perimetro.h"

using std::cin;
using std::cout;
using std::endl;

float calcperimetrotriangulo(){
	float lado;
	cout << "Para calcular o perimetro do triangulo equilatero, digite a medida do lado: " << endl;
	cin >> lado;
	
	return perimetrotriangulo(lado);
}

float calcperimetroretangulo(){
	float base, altura;
	cout << "Para calcular o perimetro do retangulo, digite a medida da base: " << endl;
	cin >> base;
	cout << "Para calcular o perimetro do retangulo, digite a medida da altura: " << endl;
	cin >> altura;
	
	return perimetroretangulo(base, altura);
}

float calcperimetroquadrado(){
    
	float lado;
	cout << "Para calcular o perimetro do quadrado, digite a medida do lado: " << endl;
	cin >> lado;	
	return perimetroquadrado(lado);
}

float calcperimetrocirculo(){
	float raio;
	cout << "Para calcular o perimetro do círculo, digite a medida do raio: " << endl;
	cin >> raio;	
	return perimetrocirculo(raio);
}
