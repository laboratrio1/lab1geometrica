#include <iostream>
#include <cstdlib>
#include <cmath>
#include "volume.h"

float volumepiramide (float areabase, float altura){
	float resultado;
	resultado = (1 / 3) * (areabase * altura);
	return resultado;
}

float volumecubo (float aresta){
	float resultado;
	resultado = (pow(3, aresta));
	return resultado;
}

float volumeparalelepipedo (float aresta1,float aresta2,float aresta3){
	float resultado;
	resultado = aresta1 * aresta2 * aresta3;
	return resultado;
}
float volumeesfera (float raio){
	float pi = 3.1415;
    float resultado;
    resultado = (4* (pi * pow(3,raio))/3);
    return resultado;
}
