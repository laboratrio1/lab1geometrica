#include <iostream>
#include "calcvolume.h"
#include "volume.h"

using std::cin;
using std::cout;
using std::endl;

float calcvolumepiramide(){
	float areabase, altura;
	cout << "Para calcular o volume da pirâmide, digite a área de sua base: " << endl;
	cin >> areabase;
	cout << "Para calcular o volume da pirâmide, digite a altura dela: " << endl;
	cin >> altura;
	return volumepiramide(areabase, altura);
}

float calcvolumecubo(){
	float aresta;
	cout << "Para calcular o volume do cubo, digite a medida da aresta: " << endl;
	cin >> aresta;
	return volumecubo(aresta);
}

float calcvolumeparalelepipedo(){
	float aresta1, aresta2, aresta3;
	cout << "Para calcular o volume do paralelepípedo, digite a medida da 1ª aresta: " << endl;
	cin >> aresta1;
	cout << "Para calcular o volume do paralelepípedo, digite a medida da 2ª aresta: " << endl;
	cin >> aresta2;
	cout << "Para calcular o volume do paralelepípedo, digite a medida da 3ª aresta: " << endl;
	cin >> aresta3;
	return volumeparalelepipedo(aresta1, aresta2, aresta3);
}
float calcvolumeesfera(){
	float raio;
	cout << "Para calcular o volume da esfera, digite a área do seu raio: " << endl;
	cin >> raio;
	return volumeesfera(raio);
}