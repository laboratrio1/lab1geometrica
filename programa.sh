#!/bin/bash

mkdir -p bin/
mkdir -p build/
mkdir -p doc/
mkdir -p include/
mkdir -p lib/
mkdir -p src/
mkdir -p test/
touch makefile
touch .gitignore
